module Main where

import           Protolude
import           Network.Matrix


getToken :: IO (Maybe Token)
getToken = getArgs >>= \a -> return $ Token . toS <$> head a


roomId :: RoomId
roomId = "!qYqlNOmvFNvqroBfQW:basking"


printResponse :: (Show a, MonadIO m) => Either ClientError a -> m ()
printResponse = either print print


config :: BaseUrl -> Token -> ClientConfig
config uri t = ClientConfig { serverUri = uri, serverToken = t }


main :: IO ()
main = do
  token <- getToken

  case token of
    Nothing     -> putStrLn ("Usage: matrix-client <token>" :: Text)
    Just token' ->
      void $ runMatrixT (config localMatrixURI token') $ do

        printResponse =<< matrix versions
        printResponse =<< matrix whoami

        printResponse =<< matrix . sendMsgEvent roomId (formattedMsg m1 m2) =<< generateTxId
        printResponse =<< matrix . sendMsgEvent roomId (plainMsg m1) =<< generateTxId

        where
          m1 = "Now this is not a formatted message" :: Text
          m2 = "Now this is a <em>formatted</em> message" :: Text
