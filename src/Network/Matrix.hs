{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE FlexibleContexts           #-}

{-# LANGUAGE PartialTypeSignatures      #-}

module Network.Matrix (
      ClientConfig(..)
    , MatrixT
    , runMatrixT
    , defaultMatrixURI
    , localMatrixURI
    , matrix
    , formattedMsg
    , plainMsg
    , generateTxId
    , module Network.Matrix.API
    , module X
    ) where

import           Protolude

import           Data.Time

import           Network.HTTP.Client.TLS    (tlsManagerSettings)
import           Network.HTTP.Client (newManager)
import           Servant.Client (BaseUrl(..), ClientEnv, runClientM, Scheme(..), ClientError, ClientM, showBaseUrl, parseBaseUrl, mkClientEnv)

import           Control.Monad.Trans.Class (MonadTrans)

import qualified Servant.Client as X (ClientError(..), Scheme(..), BaseUrl(..), showBaseUrl, parseBaseUrl)

import           Network.Matrix.API


data ClientConfig = ClientConfig {
    serverUri :: BaseUrl
  , serverToken :: Token
  } deriving Show


newtype MatrixT m a = MatrixT (
    ReaderT ClientConfig (ExceptT ClientError m) a
  ) deriving (Functor, Applicative, Monad, MonadIO, MonadError ClientError,
              MonadReader ClientConfig)


instance MonadTrans MatrixT where
  lift = MatrixT . lift .lift


-- | Construct a formatted mssage using org.matrix.custom.html formatting.
--   Note: it's up to the user to properly format the message
formattedMsg :: (ConvertText t Text) => t -> t -> SendMsgEventRequest
formattedMsg msg fmtMsg = SendMsgEventRequest { _msgtype = METext, _format = Just MessageFormat,
                                                _body = toS msg, _formatted_body = Just (toS fmtMsg) }


-- | Construct a plain message.
plainMsg :: (ConvertText t Text) => t -> SendMsgEventRequest
plainMsg msg = SendMsgEventRequest { _msgtype = METext, _format = Nothing,
                                     _body = toS msg, _formatted_body = Nothing }


-- | The main matrix.org URI
defaultMatrixURI :: BaseUrl
defaultMatrixURI = BaseUrl Https "matrix.org" 8448 ""


-- | Local matrix URI. Mainly for testing.
localMatrixURI :: BaseUrl
localMatrixURI = BaseUrl Http "localhost" 8008 ""


tlsManager :: BaseUrl -> IO ClientEnv
tlsManager uri = flip mkClientEnv uri <$> newManager tlsManagerSettings


-- | Generate a transaction id. A unique transaction id must be generated for
--   every message. The Matrix homeserver uses this to disambiguate messages
--   so if you reuse a transaction id you will appear to lose messages.
generateTxId :: MonadIO m => MatrixT m TxnId
generateTxId =
  liftIO $ TxnId . toS . formatTime defaultTimeLocale "%s%Q" <$> getCurrentTime


runMatrixT :: ClientConfig -> MatrixT m a -> m (Either ClientError a)
runMatrixT config (MatrixT mClient) =
  runExceptT (runReaderT mClient config)


-- | Runs a Matrix endpoint.
matrix :: MonadIO m => (Maybe Token -> ClientM a) -> MatrixT m (Either ClientError a)
matrix endpoint = do
  conf <- ask
  liftIO $ go (serverUri conf) (endpoint (Just $ serverToken conf))

  where
    go serverUri' conf' = runClientM conf' =<< tlsManager serverUri'
