{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Network.Matrix.Types where

import           Protolude

import           Data.Aeson
import           Aeson.Util

import           Web.HttpApiData


newtype RoomId = RoomId Text deriving (ToHttpApiData, FromHttpApiData, Show, IsString)

newtype Token = Token Text deriving (ToHttpApiData, FromHttpApiData, Show, IsString)

newtype TxnId = TxnId Text deriving (ToHttpApiData, FromHttpApiData, Show, Eq, IsString)

newtype EventId = EventId Text deriving (ToHttpApiData, FromHttpApiData, Show, IsString)

data MessageFormat = MessageFormat deriving (Generic, Show)


data MsgEvent = METext | MEEmote | MENotice | MEImage | MEFile | MELocation | MEVideo | MEAudio deriving (Generic, Show)

data InstantMsgEvent = IMmessage | IMmessageFeedback | IMname | IMtopic | IMavatar | IMpinnedEvents deriving (Show)


instance ToHttpApiData InstantMsgEvent where
  toUrlPiece IMmessage = "m.room.message"
  toUrlPiece IMmessageFeedback = "m.room.message.feedback"
  toUrlPiece IMname = "m.room.name"
  toUrlPiece IMtopic = "m.room.topic"
  toUrlPiece IMavatar = "m.room.avatar"
  toUrlPiece IMpinnedEvents = "m.room.pinned_events"


instance FromJSON MsgEvent


instance ToJSON MsgEvent where
  toJSON METext = "m.text"
  toJSON MEEmote = "m.emote"
  toJSON MENotice = "m.notice"
  toJSON MEImage = "m.image"
  toJSON MEFile = "m.file"
  toJSON MELocation = "m.location"
  toJSON MEVideo = "m.video"
  toJSON MEAudio = "m.audio"


instance FromJSON MessageFormat

instance ToJSON MessageFormat where
  toJSON MessageFormat = "org.matrix.custom.html"


data SendMsgEventRequest = SendMsgEventRequest {
    _msgtype :: MsgEvent
  , _body :: Text
  , _format :: Maybe MessageFormat
  , _formatted_body :: Maybe Text
  } deriving (Show, Generic)


data WhoAmIResponse = WhoAmIResponse {
    _Wuser_id :: Text
  } deriving (Show, Generic)


data VersionsResponse = VersionsResponse {
  _Vversions :: [Text]
  } deriving (Show, Generic)


data RoomEventResponse = RoomEventResponse {
  _REevent_id :: Text
  } deriving (Show, Generic)


flip deriveJSON ''SendMsgEventRequest $ dropCharsfromLabel 1

flip deriveJSON ''WhoAmIResponse $ dropCharsfromLabel 2
flip deriveJSON ''VersionsResponse $ dropCharsfromLabel 2
flip deriveJSON ''RoomEventResponse $ dropCharsfromLabel 3
