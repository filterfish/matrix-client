{-# LANGUAGE DataKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}


module Network.Matrix.API (
    module Network.Matrix.Types
  , whoami
  , versions
  , sendMsgEvent'
  , sendMsgEvent
  ) where

import           Protolude

import           Servant.API
import           Servant.Client

import           Network.Matrix.Types



type VersionsAPI = "_matrix" :> "client" :> "versions"
                 :> QueryParam "access_token" Token
                 :> Get '[JSON] VersionsResponse


type CurrentAccontInfo = "_matrix" :> "client" :> "r0" :> "account"  :> "whoami"
                       :> QueryParam "access_token" Token
                       :> Get '[JSON] WhoAmIResponse


-- See: https://matrix.org/docs/spec/client_server/r0.3.0.html#put-matrix-client-r0-rooms-roomid-send-eventtype-txnid
--
type SendMsgEvent = "_matrix" :> "client" :> "api" :> "v1" :> "rooms"
                  :> Capture "roomId" RoomId
                  :> "send"
                  :> Capture "eventType" InstantMsgEvent -- "m.room.message"
                  :> ReqBody '[JSON] SendMsgEventRequest
                  :> Capture "txnId" TxnId
                  :> QueryParam "access_token" Token
                  :> Put '[JSON] RoomEventResponse


sendMsgEvent' :: RoomId -> InstantMsgEvent -> SendMsgEventRequest -> TxnId -> Maybe Token -> ClientM RoomEventResponse


sendMsgEvent :: RoomId -> SendMsgEventRequest -> TxnId -> Maybe Token -> ClientM RoomEventResponse
sendMsgEvent roomId' = sendMsgEvent' roomId' IMmessage


whoami :: Maybe Token -> ClientM WhoAmIResponse


versions :: Maybe Token -> ClientM VersionsResponse


type API = CurrentAccontInfo :<|> SendMsgEvent :<|> VersionsAPI


whoami :<|> sendMsgEvent' :<|> versions = client api


api :: Proxy API
api = Proxy
