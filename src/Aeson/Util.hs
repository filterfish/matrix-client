module Aeson.Util (
    module Data.Aeson.TH
  , aesonDefaultOptions
  , dropCharsfromLabel
  ) where

import           Protolude
import           Data.Aeson.TH

import           Control.Lens

-- This came from:
--  http://stackoverflow.com/questions/42042761/how-to-automatically-convert-a-record-in-makefields-lens-format-to-json-with-f

aesonDefaultOptions :: Int -> Options
aesonDefaultOptions n = defaultOptions {fieldLabelModifier = (_head %~ toLower) . drop n}


-- This came from the Aeson docs: https://hackage.haskell.org/package/aeson-2.1.2.1/docs/Data-Aeson-TH.html
dropCharsfromLabel :: Int -> Options
dropCharsfromLabel n = defaultOptions{fieldLabelModifier = drop n, constructorTagModifier = map toLower}
